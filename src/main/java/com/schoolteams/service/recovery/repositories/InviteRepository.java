package com.schoolteams.service.recovery.repositories;

import org.springframework.http.HttpStatus;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class InviteRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection conn = null;

    /**
     * Get a specific user
     * @param username The users username
     * @return The user
     */
    public String getSpecificUsersEmail(String username){
        String email = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT email" +
                    " FROM public.users" +
                    " INNER JOIN person" +
                    " ON person.id = users.person_id" +
                    " WHERE username = ?;");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                email = result.getString("email");
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return email;
    }

    public HttpStatus toggleInvited(String username){
        HttpStatus success;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("UPDATE public.users SET invited=true" +
                            " WHERE username = ?;");
            prep.setString(1, username);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.OK;
            }else{
                success = null;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            success = null;
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

}
