package com.schoolteams.service.recovery.repositories;

import org.springframework.http.HttpStatus;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class RecoverRepository {
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection conn = null;

    /**
     * Get a specific user
     * @param username The users username
     * @return The user
     */
    public String getSpecificUsersEmail(String username){
        String email = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT email" +
                    " FROM public.users" +
                    " INNER JOIN person" +
                    " ON person.id = users.person_id" +
                    " WHERE username = ?;");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                email = result.getString("email");
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return email;
    }

    /**
     * Updates password
     * @param username The username to the user to update password
     * @param password The new password
     * @return The response status
     */
    public HttpStatus updatePassword(String username, String password){

        HttpStatus success;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("UPDATE public.users SET password=?" +
                            " WHERE username = ?;");
            prep.setString(1, password);
            prep.setString(2, username);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.ACCEPTED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }


    /**
     * Get a specific users qr code url
     * @param username The users username
     * @return The url
     */
    public String getSpecificUsersQrCodeUrl(String username){
        String url = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT tfa_qr_url, \"2fa_active\"" +
                    " FROM public.users" +
                    " WHERE username = ?;");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                if(result.getBoolean("2fa_active")){
                    url = result.getString("tfa_qr_url");
                }
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return url;
    }
}
