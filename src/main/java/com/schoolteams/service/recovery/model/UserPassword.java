package com.schoolteams.service.recovery.model;

public class UserPassword {
    private String password;
    private String token;

    public UserPassword(String password, String token) {
        this.password = password;
        this.token = token;
    }

    public UserPassword() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
