package com.schoolteams.service.recovery.controller;

import com.google.common.hash.Hashing;
import com.schoolteams.service.recovery.jwt.JwtKey;
import com.schoolteams.service.recovery.mail.MailService;
import com.schoolteams.service.recovery.model.User;
import com.schoolteams.service.recovery.model.UserPassword;
import com.schoolteams.service.recovery.repositories.RecoverRepository;
import io.jsonwebtoken.Claims;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.nio.charset.StandardCharsets;

@RestController
@RequestMapping("/api/v1/recover")
@CrossOrigin("*")
public class RecoverController {

    private RecoverRepository recoverRepository = new RecoverRepository();

    @PostMapping()
    public ResponseEntity<String> recover(@RequestBody User user){
        MailService mailService = new MailService();
        String reciever = recoverRepository.getSpecificUsersEmail(user.getUsername());
        if(reciever == null){
            return new ResponseEntity<>("No user found", HttpStatus.NOT_FOUND);
        }

        String qrCodeUrl = recoverRepository.getSpecificUsersQrCodeUrl(user.getUsername());

        HttpStatus success = mailService.sendRecover(reciever, user.getUsername(), qrCodeUrl);

        if(success == HttpStatus.OK){
            return new ResponseEntity<>("Email sent.", HttpStatus.OK);
        }
        return new ResponseEntity<>(null, success);
    }

    @PostMapping("/change")
    public ResponseEntity<String> change(@RequestBody UserPassword user){

        try{
            Claims claim = JwtKey.decodeJWT(user.getToken());
            String username = claim.getSubject();
            if(!claim.getIssuer().equals(System.getenv("JWT_ISSUER"))){
                return new ResponseEntity<>("Bad request", HttpStatus.BAD_REQUEST);
            }

            String hashedPassword = Hashing.sha256()
                    .hashString(user.getPassword(), StandardCharsets.UTF_8)
                    .toString();

            HttpStatus success = recoverRepository.updatePassword(username, hashedPassword);

            if(success != HttpStatus.ACCEPTED){
                return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>("Password changed", HttpStatus.OK);

        }catch (Exception e){
            return new ResponseEntity<>("Bad request", HttpStatus.BAD_REQUEST);
        }
    }
}
