package com.schoolteams.service.recovery.controller;

import com.schoolteams.service.recovery.mail.MailService;
import com.schoolteams.service.recovery.model.User;
import com.schoolteams.service.recovery.repositories.InviteRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/invite")
@CrossOrigin("*")
public class InviteController {

    private InviteRepository inviteRepository = new InviteRepository();
    private MailService mailService = new MailService();

    @PostMapping()
    public ResponseEntity<String> invite(@RequestBody User user){

        String email = inviteRepository.getSpecificUsersEmail(user.getUsername());

        if(email == null ){
            return new ResponseEntity<>("No email found", HttpStatus.NOT_FOUND);
        }

        HttpStatus invited = inviteRepository.toggleInvited(user.getUsername());
        if(invited == null){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        HttpStatus success = mailService.sendInvite(email, user.getUsername());

        if(success == HttpStatus.OK){
            return new ResponseEntity<>("Email sent.", HttpStatus.OK);
        }
        return new ResponseEntity<>(null, success);
    }
}
