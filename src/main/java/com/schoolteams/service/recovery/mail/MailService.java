package com.schoolteams.service.recovery.mail;

import com.schoolteams.service.recovery.jwt.JwtKey;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.util.FileCopyUtils;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.*;
import java.util.Properties;

public class MailService {

    public HttpStatus sendInvite(String recipientEmail, String username){
        return sendMail("mail_invite.txt", "Welcome to School Teams!", recipientEmail, username, null);
    }

    public HttpStatus sendRecover(String recipientEmail, String username, String qrCodeUrl){
        return sendMail("mail_recover.txt", "Reset password for School Teams", recipientEmail, username, qrCodeUrl);
    }


    private String readFile(String filename, String username, String token, String qrCodeUrl){
        //Variables to change in the mail
        String email;
        String actionUrl = "https://school-teams.herokuapp.com/resetpw/" + token; // {{actionUrl}}
        String productUrl = "https://school-teams.herokuapp.com/"; // {{productUrl}}
        String productName = "School Teams"; // [Product Name]

        try{
            ResourceLoader resourceLoader = new DefaultResourceLoader();
            Resource resource = resourceLoader.getResource("classpath:" + filename);
            Reader reader = new InputStreamReader(resource.getInputStream());

            email = FileCopyUtils.copyToString(reader);

            String correctMail;
            String s = email
                    .replaceAll("-name-", username)
                    .replaceAll("-action_url-", actionUrl)
                    .replaceAll("-product_url-", productUrl)
                    .replaceAll("-ProductName-", productName);
            if(qrCodeUrl != null){ //Recover email
                correctMail = s
                        .replaceAll("-qrCodeUrl-",
                                "<tr>\n" +
                                                "<td align=\"center\">\n" +
                                                    "<p>Your google authenticator qr code. Scan it with the application on your phone.</p>" +
                                                "</td>" +
                                            "</tr>" +
                                            "<tr>" +
                                                "<td align=\"center\">" +
                                                    "<img src='"+qrCodeUrl+"'>" +
                                                "</td>" +
                                            "</tr>");
            }else{ //Invite mail
                correctMail = s
                        .replaceAll("-qrCodeUrl-", "");
            }

            return correctMail;

        }catch(FileNotFoundException ex){
            System.out.println("\u001B[31m\nSorry, could not find the file\n\u001B[0m");
        }catch(IOException ex){
            System.out.println("\u001B[31m\nSorry, this file does not exist\n\u001B[0m");
        }

        return null;
    }

    private HttpStatus sendMail(String filename, String subject, String recipientEmail, String username, String qrCodeUrl){

        // Sender's email ID needs to be mentioned
        String from = "noroff.schoolteams@gmail.com";
        String password = System.getenv("GMAIL_PW");

        String token = JwtKey.createJWT("RecoverPassword", username, 14400000);

        // Assuming you are sending email from through gmails smtp
        String host = "smtp.gmail.com";

        // Get system properties
        Properties properties = System.getProperties();

        // Setup mail server
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.smtp.auth", "true");

        // Get the Session object.// and pass username and password
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {

            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }

        });

        // Used to debug SMTP issues
        //session.setDebug(true);

        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipientEmail));

            // Set Subject: header field
            message.setSubject(subject);

            String mail = readFile(filename, username, token, qrCodeUrl);

            message.setContent(
                    mail,
                    "text/html");

            // Send message
            Transport.send(message);
            return HttpStatus.OK;

        } catch (MessagingException mex) {
            mex.printStackTrace();
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }

    }

}
